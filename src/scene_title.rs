extern crate sdl2;

use sdl2::EventPump;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::render::{Canvas, Texture};
use sdl2::video::Window;
use sdl2::image::LoadTexture;

use assets::images;
use scenes::{Scene, SceneMove};

pub struct SceneTitle {
    menu_i: u8,
}

impl Scene for SceneTitle {
    fn run(&mut self, canvas: &mut Canvas<Window>, event_pump: &mut EventPump) -> SceneMove {
        let texture_creator = canvas.texture_creator();
        let img_background = texture_creator
            .load_texture(images::path(images::TITLE_BG).as_path())
            .unwrap();
        let mut events = event_pump.poll_iter();

        'sceneloop: loop {
            for event in &mut events {
                match event {
                    Event::Quit { .. } => return SceneMove::StopScene,
                    Event::KeyDown { keycode: Some(key), .. } => {
                        match key {
                            Keycode::Escape => return SceneMove::StopScene,
                            _ => {}
                        }
                    }
                    _ => {}
                }
            }

            // DRAW
            canvas.copy(&img_background, None, None);
            canvas.present();
        }

        return SceneMove::SameScene;
    }
}

impl SceneTitle {
    pub fn new() -> SceneTitle {
        SceneTitle { menu_i: 0 }
    }
}
