pub mod images {
    use std::path::{Path, PathBuf};

    type Element = str;

    const PATH: &str = "assets/imgs/";
    pub static TITLE_BG: &Element = "swamp_by_chibionpu-db8yjb3.png";

    pub fn path(elem: &Element) -> PathBuf {
        let mut path = PathBuf::from(PATH);
        path.push(elem);
        return path;
    }
}
