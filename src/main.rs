extern crate sdl2;

use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::image::{INIT_PNG, INIT_JPG};

mod assets;
mod scenes;
mod scene_title;

use scenes::{Scene, SceneMove, KnownScene};
use scene_title::SceneTitle;

fn main() {
    // Load library systems
    let sdl = sdl2::init().unwrap();
    let image = sdl2::image::init(INIT_PNG | INIT_JPG).unwrap();
    let ttf = sdl2::ttf::init().unwrap();
    let video = sdl.video().unwrap();
    let mut event_pump = sdl.event_pump().unwrap();

    // Create window
    let window = video
        .window("Infinity Combo Fighters", 960, 540)
        .position_centered()
        .build()
        .unwrap();

    // Create accelerated canvas
    let mut canvas = window.into_canvas().accelerated().build().unwrap();
    canvas.set_draw_color(sdl2::pixels::Color::RGBA(0, 0, 0, 255));

    // Initial scene
    let mut scene = KnownScene::Title(SceneTitle::new());

    // Mainloop
    'mainloop: loop {
        match scene.run(&mut canvas, &mut event_pump) {
            SceneMove::StopScene => break 'mainloop,
            SceneMove::GoNextScene(scn) => scene = scn,
            _ => {}
        }
    }
}
