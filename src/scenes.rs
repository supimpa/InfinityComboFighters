extern crate sdl2;

use sdl2::EventPump;
use sdl2::render::Canvas;
use sdl2::video::Window;

pub trait Scene {
    fn run(&mut self, canvas: &mut Canvas<Window>, event_pump: &mut EventPump) -> SceneMove;
}

pub enum SceneMove {
    StopScene,
    SameScene,
    GoNextScene(KnownScene),
}

use scene_title::SceneTitle;

pub enum KnownScene {
    Title(SceneTitle),
}

impl Scene for KnownScene {
    fn run(&mut self, canvas: &mut Canvas<Window>, event_pump: &mut EventPump) -> SceneMove {
        match *self {
            KnownScene::Title(ref mut real) => real.run(canvas, event_pump),
        }
    }
}
