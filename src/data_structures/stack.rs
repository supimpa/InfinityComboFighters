use std::any::Any;

#[derive(Clone, PartialEq)]
struct BruteStackNode<T> {
    info: T,
    next: StackNode<T>,
}

type StackNode<T> = Option<Box<BruteStackNode<T>>>;

#[derive(Clone)]
struct Stack<T> {
    first: StackNode<T>,
    limit: Option<usize>,
}

impl<T: Any> Stack<T> {
    pub fn new(limit_: Option<usize>) -> Stack<T> {
        Stack {
            first: None,
            limit: limit_,
        }
    }

    pub fn is_empty(&self) -> bool {
        match self.first {
            Some(_) => false,
            None => true,
        }
    }

    pub fn len(&self) -> usize {
        match self.first {
            Some(ref first_val) => first_val.len(),
            None => 0,
        }
    }

    pub fn push(&mut self, info_: T) -> bool {
        self.first = Some(Box::new(BrutePileNode {
            info: info_,
            next: self.first.take(),
        }));
    }

    pub fn pop(&mut self) -> Result<T, &'static str> {
        match self.first.take() {
            Some(mut first_val) => {
                self.first = first_val.next.take();
                return Ok(first_val.info);
            }
            None => {
                return Err("nothing to take");
            }
        }
    }
}

impl<T: Any> BruteStackNode<T> {
    pub fn len(&self) -> usize {
        match self.next {
            Some(ref next_val) => 1 + next_val.len(),
            None => 1,
        }
    }

    pub fn add_last(&mut self, new_last: StackNode<T>) {
        match self.next {
            None => self.next = new_last,
            Some(ref mut next) => next.add_last(new_last),
        }
    }
}
