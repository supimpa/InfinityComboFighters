use std::any::Any;

#[derive(Clone, PartialEq)]
struct BruteQueueNode<T> {
    info: T,
    next: QueueNode<T>,
}

type QueueNode<T> = Option<Box<BruteQueueNode<T>>>;

#[derive(Clone)]
struct Queue<T> {
    first: QueueNode<T>,
    limit: Option<usize>,
}

impl<T: Any> Queue<T> {
    pub fn new(limit_: Option<usize>) -> Queue<T> {
        Queue {
            first: None,
            limit: limit_,
        }
    }

    pub fn is_empty(&self) -> bool {
        match self.first {
            Some(_) => false,
            None => true,
        }
    }

    pub fn len(&self) -> usize {
        match self.first {
            Some(ref first_val) => first_val.len(),
            None => 0,
        }
    }

    pub fn enqueue(&mut self, info_: T) -> bool {
        match self.limit {
            Some(ref limit_val) => {
                if self.len() >= *limit_val {
                    return false;
                }
            }
            None => {}
        }
        let new_last = Some(Box::new(BruteQueueNode {
            info: info_,
            next: None,
        }));
        match self.first {
            None => self.first = new_last,
            Some(ref mut first) => first.add_last(new_last),
        }
        return true;
    }

    pub fn dequeue(&mut self) -> Result<T, &'static str> {
        match self.first.take() {
            Some(mut first_val) => {
                self.first = first_val.next.take();
                return Ok(first_val.info);
            }
            None => {
                return Err("nothing to take");
            }
        }
    }
}

impl<T: Any> BruteQueueNode<T> {
    pub fn len(&self) -> usize {
        match self.next {
            Some(ref next_val) => 1 + next_val.len(),
            None => 1,
        }
    }

    pub fn add_last(&mut self, new_last: QueueNode<T>) {
        match self.next {
            None => self.next = new_last,
            Some(ref mut next) => next.add_last(new_last),
        }
    }
}
