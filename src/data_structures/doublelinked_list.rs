use std::any::Any;
use std::ptr;
use std::mem::size_of;
use std::slice;

#[derive(Clone, PartialEq)]
struct BruteListNode<T> {
    info: T,
    prev: *mut BruteListNode<T>,
    next: ListNode<T>,
}

type ListNode<T> = Option<Box<BruteListNode<T>>>;

#[derive(Clone)]
struct List<T> {
    head: ListNode<T>,
    limit: Option<usize>,
}

impl<T: Any> List<T> {
    pub fn new(limit_: Option<usize>) -> List<T> {
        List {
            head: None,
            limit: limit_,
        }
    }

    pub fn is_empty(&self) -> bool {
        match self.head {
            Some(_) => false,
            None => true,
        }
    }

    pub fn len(&self) -> usize {
        match self.head {
            Some(ref head_val) => head_val.len(),
            None => 0,
        }
    }

    pub fn append(&mut self, info_: T) -> bool {
        match self.limit {
            Some(ref limit_val) => {
                if self.len() >= *limit_val {
                    return false;
                }
            }
            None => {}
        }
        match self.head {
            None => {
                self.head = Some(Box::new(BruteListNode {
                    info: info_,
                    prev: ptr::null_mut(),
                    next: None,
                }))
            }
            Some(ref mut head) => head.add_last(info_),
        }
        return true;
    }

    pub fn take(&mut self) -> Result<T, &'static str> {
        match self.head.take() {
            Some(mut head_val) => {
                self.head = head_val.next.take();
                return Ok(head_val.info);
            }
            None => {
                return Err("nothing to take");
            }
        }
    }
}

impl<T: Any> BruteListNode<T> {
    pub fn len(&self) -> usize {
        match self.next {
            Some(ref next_val) => 1 + next_val.len(),
            None => 1,
        }
    }

    pub fn add_last(&mut self, new_last_info: T) {
        match self.next {
            None => {
                self.next = Some(Box::new(BruteListNode {
                    info: new_last_info,
                    prev: self as *mut BruteListNode<T>,
                    next: None,
                }))
            }
            Some(ref mut next) => next.add_last(new_last_info),
        }
    }

    pub fn get_prev(&self) -> ListNode<T> {
        if self.prev == ptr::null_mut() {
            None
        } else {
            unsafe {
                Some(Box::from_raw(
                    slice::from_raw_parts_mut(
                        self.prev,
                        size_of::<BruteListNode<T>>(),
                    ).as_mut_ptr(),
                ))
            }
        }
    }
}
