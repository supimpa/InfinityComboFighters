use std::fmt;
use std::any::Any;

// Implements Binary Trees
#[derive(Clone)]
struct BruteBinaryTree<T> {
    info: T,
    left: BinaryTree<T>,
    right: BinaryTree<T>,
}

#[derive(Clone)]
enum BinaryTree<T> {
    Empty,
    Just(Box<BruteBinaryTree<T>>),
}

impl<T: Any> BinaryTree<T> {
    pub fn new(info_: T) -> BinaryTree<T> {
        BinaryTree::Just(Box::new(BruteBinaryTree {
            info: info_,
            left: BinaryTree::Empty,
            right: BinaryTree::Empty,
        }))
    }
}

impl<T: Any + fmt::Display> fmt::Display for BinaryTree<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            BinaryTree::Just(ref r) => {
                return write!(f, "[{}](L: {}, R: {})", r.info, r.left, r.right);
            }
            BinaryTree::Empty => {
                return write!(f, "-");
            }
        }
    }
}

// Implements Searchable Binary Trees
// T: Any + Ord
impl<T: Any + Ord> BinaryTree<T> {
    pub fn insert(&mut self, info_: T) -> bool {
        match *self {
            BinaryTree::Just(ref mut r) => {
                if r.info == info_ {
                    return false;
                } else if info_ > r.info {
                    return r.right.insert(info_);
                } else {
                    return r.left.insert(info_);
                }
            }
            BinaryTree::Empty => {
                *self = BinaryTree::new(info_);
                return true;
            }
        }
    }
}

// Implements Searchable Binary Trees as IMMUTABLEs
impl<T: Any + Ord + Clone> BinaryTree<T> {
    pub fn insert_(&self, info_: T) -> BinaryTree<T> {
        match *self {
            BinaryTree::Just(ref r) => {
                if r.info == info_ {
                    return (*self).clone();
                } else if info_ > r.info {
                    return BinaryTree::Just(Box::new(BruteBinaryTree {
                        info: r.info.clone(),
                        left: r.left.clone(),
                        right: BinaryTree::insert_(&r.right.clone(), info_),
                    }));
                } else {
                    return BinaryTree::Just(Box::new(BruteBinaryTree {
                        info: r.info.clone(),
                        left: BinaryTree::insert_(&r.left.clone(), info_),
                        right: r.right.clone(),
                    }));
                }
            }
            BinaryTree::Empty => {
                return BinaryTree::new(info_);
            }
        }
    }
}
